@echo off

for /f "tokens=4 delims= " %%i IN ('powercfg -GETACTIVESCHEME') do Set Scheme=%%i

powercfg -SETACVALUEINDEX %Scheme% SUB_BUTTONS LIDACTION 000
powercfg -SETDCVALUEINDEX %Scheme% SUB_BUTTONS LIDACTION 000
powercfg /s %Scheme%

tasklist /nh /fi "imagename eq RemoteMouse.exe" | find /i "RemoteMouse.exe" > nul || (start "" "C:\Program Files (x86)\Remote Mouse\RemoteMouse.exe")

PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& '%~dp0MagnifyCursor.ps1'";

echo "Sleep when lid is closed" DISABLED!
echo Press any key to re-enable...
pause >nul

powercfg -SETACVALUEINDEX %Scheme% SUB_BUTTONS LIDACTION 001
powercfg -SETDCVALUEINDEX %Scheme% SUB_BUTTONS LIDACTION 001
powercfg /s %Scheme%

PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& '%~dp0DefaultCursor.ps1'";