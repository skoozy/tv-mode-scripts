@echo off

for /f "tokens=4 delims= " %%i IN ('powercfg -GETACTIVESCHEME') do Set Scheme=%%i

powercfg -SETACVALUEINDEX %Scheme% SUB_BUTTONS LIDACTION 000
powercfg -SETDCVALUEINDEX %Scheme% SUB_BUTTONS LIDACTION 000
powercfg /s %Scheme%

echo "Sleep when lid is closed" DISABLED!
echo Press any key to re-enable...
pause >nul

powercfg -SETACVALUEINDEX %Scheme% SUB_BUTTONS LIDACTION 001
powercfg -SETDCVALUEINDEX %Scheme% SUB_BUTTONS LIDACTION 001
powercfg /s %Scheme%