$RegConnect = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]"CurrentUser","$env:COMPUTERNAME")

$RegCursors = $RegConnect.OpenSubKey("Control Panel\Cursors",$true)



$RegCursors.SetValue("","Windows Default")

$RegCursors.SetValue("AppStarting","%SystemRoot%\cursors\aero_working.ani")

$RegCursors.SetValue("Arrow","%SystemRoot%\cursors\aero_arrow.cur")

$RegCursors.SetValue("Crosshair","")

$RegCursors.SetValue("Hand","%SystemRoot%\cursors\aero_link.cur")

$RegCursors.SetValue("Help","%SystemRoot%\cursors\aero_helpsel.cur")

$RegCursors.SetValue("IBeam","")

$RegCursors.SetValue("No","%SystemRoot%\cursors\aero_unavail.cur")

$RegCursors.SetValue("NWPen","%SystemRoot%\cursors\aero_pen.cur")

$RegCursors.SetValue("SizeAll","%SystemRoot%\cursors\aero_move.cur")

$RegCursors.SetValue("SizeNESW","%SystemRoot%\cursors\aero_nesw.cur")

$RegCursors.SetValue("SizeNS","%SystemRoot%\cursors\aero_ns.cur")

$RegCursors.SetValue("SizeNWSE","%SystemRoot%\cursors\aero_nwse.cur")

$RegCursors.SetValue("SizeWE","%SystemRoot%\cursors\aero_ew.cur")

$RegCursors.SetValue("UpArrow","%SystemRoot%\cursors\aero_up.cur")

$RegCursors.SetValue("Wait","%SystemRoot%\cursors\aero_busy.ani")



$RegCursors.Close()

$RegConnect.Close()

$CSharpSig = @'

[DllImport("user32.dll", EntryPoint = "SystemParametersInfo")]

public static extern bool SystemParametersInfo(

                 uint uiAction,

                 uint uiParam,

                 uint pvParam,

                 uint fWinIni);

'@

$CursorRefresh = Add-Type -MemberDefinition $CSharpSig -Name WinAPICall -Namespace SystemParamInfo �PassThru

$CursorRefresh::SystemParametersInfo(0x0057,0,$null,0)

# If running in the console, wait for input before closing.
#if ($Host.Name -eq "ConsoleHost")
#{
#    Write-Host "Press any key to continue..."
#    $Host.UI.RawUI.FlushInputBuffer()   # Make sure buffered input doesn't "press a key" and skip the ReadKey().
#    $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp") > $null
#}